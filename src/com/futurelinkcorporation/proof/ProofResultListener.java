package com.futurelinkcorporation.proof;

/**
 *  Interface that handles the result obtained from a Proof test.
 * @author Robert Peralta
 */
public interface ProofResultListener {
    
    public void handleResult(ProofResult result);
    
}
