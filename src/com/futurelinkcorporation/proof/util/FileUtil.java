package com.futurelinkcorporation.proof.util;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;


/**
 * Utility class to handle files.
 * @author Robert Peralta
 */
public class FileUtil {

    public static File tempFromBytes(byte[] data,String name, String extension) throws FileNotFoundException, IOException {
        
        File temp = File.createTempFile("proof_", name+extension);
        try (FileOutputStream os = new FileOutputStream(temp)) {
            os.write(data);
            os.flush();
        }
//        file.
        return temp;
        
    }

    public static File loadFile(String filename) throws FileNotFoundException {
        return new File(filename);
    }
    
}
