package com.futurelinkcorporation.proof.util;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Random;
import javax.imageio.ImageIO;

/**
 * Utilitary class for handling images.
 * @author Robert Peralta
 */
public class ImageUtil {

    /**
     * Signs the Image passed as a ByteArray a random number of times
     * at random locations with the specified signatures.
     * @param image ByteArray of ImageFile
     * @param signatures to be written.
     * @return
     * @throws IOException 
     */
    public static byte[] signImage(byte[] image, String... signatures) throws IOException {

        Random r = new Random();
        return signImage(image,10+r.nextInt(100),-1,-1,signatures);
    }

    /**
     * Sign the image passed as a byte array the specified number of times
     * with the passed signatures at the desired location.
     * For random positioning fo the signature pass -1 as the x  y values.
     * @param image
     * @param repetitions
     * @param x in case this value is -1 then it will be random.
     * @param y in case this value is -1 then it will be random.
     * @param signatures
     * @return
     * @throws IOException 
     */
    private static byte[] signImage(byte[] image, int repetitions, int x, int y
            ,String... signatures) throws IOException 
    {
        Random r = new Random();
        r.setSeed(System.currentTimeMillis());
        
        ByteArrayInputStream is = new ByteArrayInputStream(image);
        BufferedImage img = ImageIO.read(is);
    
        Graphics2D g = (Graphics2D) img.getGraphics();
        
        
        for(int i=0;i< repetitions ;i++)
        {
            for(String s : signatures)
            {
                //Old x and y values.
                int oX = x;
                int oY = y;
                
                //Let's change the font a lil'bit.
//                g.setFont( g.getFont().deriveFont(12+r.nextInt(40)) );
                
                x = x == -1 ? r.nextInt( getWritableMaxWidth(s,img,g) ) :
                              x;
                y = y == -1 ? r.nextInt( getWritableMaxHeight(s,img,g) ) :
                              y;
                Color col = randomCol(r);
                
                g.setColor(col);
                g.drawString(s, x, y);
             
                x = oX;
                y = oY;
            }
        }
        
        g.dispose();
        return bufferedImage2ByteArray(img);
    }

    /**
     * Converts BufferedImage 2 ByteArray
     * @param img
     * @return
     * @throws IOException 
     */
    private static byte[] bufferedImage2ByteArray(BufferedImage img) throws IOException {
        byte[] data;
        try (ByteArrayOutputStream os = new ByteArrayOutputStream()) {
            ImageIO.write( img, "png", os );
            os.flush();
            data = os.toByteArray();
        }
        return data;
    }

    /**
     * Based on the passed graphics and font, obtain the resulting string 
     * width when writing to the image.
     * @param s Text to be measured
     * @param img 
     * @param g 
     * @return 
     */
    private static int getWritableMaxWidth(String s, BufferedImage img, Graphics g) {
        return img.getWidth()-g.getFontMetrics().stringWidth(s);
    }

    /**
     * Based on the passed graphics and font, obtain the resulting string 
     * height when writing to the image.
     * @param s Text to be measured
     * @param img 
     * @param g 
     * @return 
     */
    private static int getWritableMaxHeight(String s, BufferedImage img, Graphics g) {
        return img.getHeight()- 
                ( 
                    g.getFontMetrics().getHeight() + 
                    g.getFontMetrics().getAscent() +
                    g.getFontMetrics().getDescent()
                );
    }

    /**
     * Return a random color with non-variable alpha.
     * @param r {@link Random} object to base the calculation on. 
     * @return 
     */
    private static Color randomCol(Random r) {
        return new Color(r.nextInt(0xFF),r.nextInt(0xFF),r.nextInt(0xFF));
    }

    /**
     * Saves an image file.
     * @param image ByteArray of the image data.
     * @param filename Full filename path Ex: C:\\test.png
     */
    public static void saveImage(byte[] image, String filename) throws IOException 
    {
        try (FileOutputStream output = new FileOutputStream(filename)) {
            output.write(image);
        }

    }

    
}
