package com.futurelinkcorporation.proof;

/**
 * Main Proof interface/abstract class that must be extended for an specific
 * Proof level.
 *
 * 
 * @author Robert Peralta
 */
public abstract class Proof {

    /**
     * ResultListener registered to this object.
     * This object's <i>handleResult</i> method will be invoked
     * after proof test is finished.
     * @see ProofResultListener
     */
    private ProofResultListener resultListener;

    public void setResultListener(ProofResultListener resultListener) {
        this.resultListener = resultListener;
    }

    /**
     * Main method that will try to pass the test.
     * It must call <i>notifyResult</i> after finished.
     * @see Proof.notifyResult.
     */
    public abstract void run();

    /**
     * Sends the specified result to the registered listener
     * invoking it's <i>handleResult</i> method.
     * @param result 
     * @see ProofResult
     */
    public void notifyResult(ProofResult result) {
        this.resultListener.handleResult(result);
    }
}
