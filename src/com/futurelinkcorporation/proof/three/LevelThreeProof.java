package com.futurelinkcorporation.proof.three;

import com.futurelinkcorporation.proof.*;
import com.futurelinkcorporation.proof.app.Application;
import com.futurelinkcorporation.proof.util.FileUtil;
import com.futurelinkcorporation.proof.util.ImageUtil;
import com.zenkey.net.prowser.*;
import java.io.*;
import java.net.URISyntaxException;
import java.util.logging.*;
import javax.xml.parsers.*;
import org.apache.commons.httpclient.Header;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.xml.sax.*;

/**
 * Level 3 proof by the time 3/31/2014 based on:
 * http://www.proveyourworth.net/level3/
 *
 * @author Robert Peralta
 */
public class LevelThreeProof extends Proof {

    private String CODE_LOCATION = "C:\\Users\\Robert Peralta\\Work\\Workspaces\\Netbeans Projects\\ProveYourWorth_3\\code.rar";
    private String CV_LOCATION = "C:\\Users\\Robert Peralta\\Work\\Workspaces\\Netbeans Projects\\ProveYourWorth_3\\CV.doc";
    private String IMAGE_DUMP_LOCATION = "C:\\Users\\Robert Peralta\\Work\\Workspaces\\Netbeans Projects\\ProveYourWorth_3\\";
    private String URI_HOME = "http://www.proveyourworth.net/level3";
    private String URI_START = "/start";
    private String URI_ACTIVATE = "/activate";
    private String PARAM_NAME_VALUE = "-1+Robert+Peralta";
    private String PARAM_NAME_KEY = "name";
    private String PARAM_HASH_KEY = "statefulhash";
    private String PARAM_HEADER_PAYLOAD = "X-Payload-URL";
    private String PARAM_HEADER_REAPER = "X-Post-Back-To";
    private String PARAM_HEADER_IMAGE_DISP = "Content-Disposition";
    private String PARAM_HEADER_IMAGE_DISP_NAME = "filename=";
    private String statefulHash;
    /**
     * For the Reaper.
     */
    private byte[] image;
    private String imageName;
    private String REAPER_PARAM_IMG = "image";
    private String REAPER_PARAM_CODE = "code";
    private String REAPER_PARAM_CV = "resume";
    private String REAPER_PARAM_EMAIL = "email";
    private String REAPER_PARAM_EMAIL_VALUE = "robert.apb@hotmail.com";
    private String REAPER_PARAM_NAME = "name";
    private String REAPER_PARAM_NAME_VALUE = "Robert Peralta";
    private String REAPER_PARAM_ABOUT = "aboutme";
    private String REAPER_PARAM_ABOUT_VALUE = "I am a Game Developer, I have a"
            + " great passion passion about it. I'm really good at wanting to know more"
            + "of what I like and THAT's what I think that makes the difference. "
            + "I'm a people person who enjoys playing tennis and playing video games. Studied Japanese "
            + "for a while too. ";

    public LevelThreeProof() {
    }

    @Override
    public void run() {

        ProofResult result = new ProofResult();


        try {

            Logger.getLogger(getClass().getSimpleName()).log(Level.INFO, "Starting LevelThreeProof");


            Prowser browser = new Prowser();
            browser.setHomePage(Request.createRequest(URI_HOME + URI_START));
            Tab tab = browser.createTab();

            Response response;

            response = login(tab);
//            System.out.println( response.getPageSource() );
            response = activate(tab);
//            System.out.println( response.getPageSource() );
            this.image = handlePayload(response);
            response = reaper(tab);
            System.out.println(response.getPageSource());
            
            response = justInCase(tab);
            
            if(response.getStatus() == 200)
            {
                result.setResultCode(ProofResult.RESULT_CODE.SUCCESS);
                result.setMessage("Completed!");
            }
            else if(response.getError() != Response.ERR_NONE)
            {
                result.setResultCode(ProofResult.RESULT_CODE.FAILED);
                result.setMessage("Failed: "+response.getErrorText());
            }

            Logger.getLogger(getClass().getSimpleName()).log(Level.INFO, "Ending LevelThreeProof");


            this.notifyResult(result);

        } catch (Exception ex) {

            Logger.getLogger(LevelThreeProof.class.getName()).log(Level.SEVERE, ex.getMessage(), ex);

            result.setResultCode(ProofResult.RESULT_CODE.ERROR);
            result.setMessage(ex.getMessage());
            this.notifyResult(result);

        }


    }

    private Response login(Tab tab) throws IOException, SAXException, ParserConfigurationException {

        Response response = tab.goHome();
        String html = response.getPageSource();

//        System.out.println(html);
        Document doc = Jsoup.parse(html);
        Element hash = doc.getElementsByAttributeValue("name", PARAM_HASH_KEY).get(0);

        this.statefulHash = hash.val();

        Request request = Request.createRequest(URI_HOME + URI_ACTIVATE);
        request.addParameter(PARAM_HASH_KEY, this.statefulHash);
        request.addParameter(PARAM_NAME_KEY, PARAM_NAME_VALUE);

        return tab.go(request);

    }

    private Response activate(Tab tab) throws URISyntaxException {

        Response response = tab.getResponse();
        Header[] headers = response.getHeaders();

        String payloadUrl = null;

        for (Header h : headers) {
//            System.out.println(h.getName() + ":" + h.getValue());
            if (h.getName().equalsIgnoreCase(PARAM_HEADER_PAYLOAD)) {
                payloadUrl = h.getValue();
            }
        }

        Request request = new Request(payloadUrl);

        return tab.go(request);
    }

    private byte[] handlePayload(Response response) throws IOException {
        byte[] image = response.getPageBytes();
        Header[] headers = response.getHeaders();
        

        
        for (Header h : headers) {
//            System.out.println(h.getName() + ":" + h.getValue());
            if (h.getName().trim().equalsIgnoreCase(PARAM_HEADER_IMAGE_DISP)) {
                this.imageName = h.getValue().split(PARAM_HEADER_IMAGE_DISP_NAME)[1].trim();
            }
        }
       
        ImageUtil.saveImage(image, IMAGE_DUMP_LOCATION+"Orig_"+this.imageName);
        image = ImageUtil.signImage(image, Application.AUTHOR, this.statefulHash);
        ImageUtil.saveImage(image, IMAGE_DUMP_LOCATION+this.imageName);
        
        return image;
    }

    private Response reaper(Tab tab) throws FileNotFoundException, IOException {
        Response response = tab.getResponse();
        Header[] headers = response.getHeaders();

        String reaperUrl = null;


        for (Header h : headers) {
//            System.out.println(h.getName() + ":" + h.getValue());
            if (h.getName().equalsIgnoreCase(PARAM_HEADER_REAPER)) {
                reaperUrl = h.getValue();
            }
        }
        
        reaperUrl = "http://127.0.0.1:8081/ProofServer/server.php";
        Request request = Request.createRequest(reaperUrl);
        request.setHttpMethod(Request.HTTP_METHOD_POST_MULTIPART);


        request.addParameter(REAPER_PARAM_IMG, FileUtil.loadFile(IMAGE_DUMP_LOCATION+this.imageName));
        request.addParameter(REAPER_PARAM_CODE, FileUtil.loadFile(CODE_LOCATION));
        request.addParameter(REAPER_PARAM_CV, FileUtil.loadFile(CV_LOCATION));
        request.addParameter(REAPER_PARAM_EMAIL, REAPER_PARAM_EMAIL_VALUE);
        request.addParameter(REAPER_PARAM_NAME, REAPER_PARAM_NAME_VALUE);
        request.addParameter(REAPER_PARAM_ABOUT, REAPER_PARAM_ABOUT_VALUE);

        return tab.go(request);
    }

    private Response justInCase(Tab tab) {
        Response response = tab.getResponse();
        Header[] headers = response.getHeaders();
        
        for (Header h : headers) {
            System.out.println(h.getName() + ":" + h.getValue());
        }
        String pageSource = response.getPageSource();
        System.out.println(pageSource);
        byte[] pageBytes = response.getPageBytes();
                
        return response;
        
    }

}