package com.futurelinkcorporation.proof.app;

import com.futurelinkcorporation.proof.Proof;
import com.futurelinkcorporation.proof.ProofResult;
import com.futurelinkcorporation.proof.ProofResultListener;
import com.futurelinkcorporation.proof.three.LevelThreeProof;
import java.util.logging.Level;
import java.util.logging.Logger;



/**
 * Main application that uses the {@link Proof} api
 * to create the test for Future Link Corporation
 * @author Robert Peralta
 */
public class Application implements ProofResultListener
{
    public static String AUTHOR = "Robert Peralta";

    public static void main(String[] args) {
        
        
        Application app = new Application();
        Proof proof = new LevelThreeProof();

        proof.setResultListener(app);
        proof.run();
        
        
        
    }
    private String MESSAGE_ERROR = "The Proof couldn't complete the test: ";
    private String MESSAGE_FAILED = "You failed the test: ";
    private String MESSAGE_SUCCESS = "You just proofed yourself: ";

    @Override
    public void handleResult(ProofResult result) {
        
        String message = "";
        
        switch(result.getResultCode())
        {
            case SUCCESS:
                message = MESSAGE_SUCCESS;
            break;
            case FAILED:
                message = MESSAGE_FAILED;
            break;
            case ERROR:
                message = MESSAGE_ERROR;
            break;
        }
        
        message += result.getMessage();
        
        Logger.getLogger(getClass().getSimpleName()).log(Level.INFO, message);
    }
    
}
