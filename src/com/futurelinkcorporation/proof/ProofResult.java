package com.futurelinkcorporation.proof;

/**
 * Contains relevant information about the result obtained from the proof.
 * @author Robert Peralta
 */
public class ProofResult {

    /**
     * Possible types of Results that a Proof can have.
     */
    public enum RESULT_CODE { 
        /*
         * The Proof passed correctly.
         */
        SUCCESS, 
        /**
         * The proof went all the way but failed somehow.
         */
        FAILED, 
        /**
         * The proof was interrupted at some point. (Use for exceptions)
         */
        ERROR 
    }
    
    private RESULT_CODE resultCode;
    

    private String message;

    public RESULT_CODE getResultCode() {
        return this.resultCode;
    }

    public String getMessage() {
        return this.message;
    }

    /**
     * @see ProofResult.RESULT_CODE
     */
    public void setResultCode(RESULT_CODE resultCode) {
        this.resultCode = resultCode;
    }

    /**
     * Message to deliver when is time to notify the listener.
     */
    public void setMessage(String message) {
        this.message = message;
    }
    
    
}
